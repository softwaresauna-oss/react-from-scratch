import React from "react";
import ReactDOM from "react-dom";
import './app.css';

class HelloMessage extends React.Component {
  render() {
    return <div className="message">Hello, {this.props.name}!</div>;
  }
}

const mountNode = document.getElementById("app");
ReactDOM.render(<HelloMessage name="Bob" />, mountNode);