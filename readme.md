# React from scratch

## Purpose

A copy-paste template for starting a React app project.

### Why not just use create-react-app?

Because it makes lots of assumptions I don't like.

Examples:

1. You will test everything with Jest
1. Your app will be in it's own Git repository
1. You don't want to tweak the Webpack config

## Usage

1. Clone this repository
1. Delete the `.git` folder to separate files from this repository
1. Continue as you wish

## Built-in scripts

- `test`: Just outputs an error message and exits; fix when you set up a test runner
- `build`: Calls `webpack`; add [Webpack options](https://webpack.js.org/api/cli/) directly in your `npm run` call with [the special `--` option](https://docs.npmjs.com/cli/run-script)
- `start`: Starts the [Webpack development server](https://webpack.js.org/guides/development/#using-webpack-dev-server) on `localhost:8080`
