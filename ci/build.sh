#!/bin/sh
#
# This is a build script for a NPM project.
#
# Parameters:
#
# 1. The path to the project root folder (the one with package.json).
#
# Requirements:
#
# 1. NPM installed.
#

cd $1

npm install
npm run build
